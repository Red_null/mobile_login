package com.example.lab2_login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.lab2_login.utils.Auth
import com.example.lab2_login.utils.User
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    lateinit var user: User

    companion object {
        const val USER_JSON = "user_json"

        fun start(context: Context, userJson: String?) {
            context.startActivity(Intent(context, MainActivity::class.java).putExtra(USER_JSON, userJson))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    fun init() {

        if (Auth.getLastLogin(this).isNullOrEmpty()) {
            Login.start(this)
            finish()
            return
        }

        var userJson = intent.getStringExtra(USER_JSON)

        if (userJson.isNullOrEmpty())
            userJson = Auth.getLastLoginUserInfo(this)

        this.user = Gson().fromJson(userJson, User::class.java)

        logout_button.setOnClickListener { logout() }

        displayUserInfo()
    }


    fun logout() {
        Auth.logout(this)
        Login.start(this, user.login)
        finish()
    }

    fun displayUserInfo() {

        login.text = user.login
        name.text = user.name
        age.text = user.age.toString()
    }
}
