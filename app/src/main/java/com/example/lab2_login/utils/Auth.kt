package com.example.lab2_login.utils

import android.content.Context
import com.google.gson.Gson

object Auth {
    const val USER_STPRAGE = "user"
    const val LAST_LOGIN = "last_login"

    fun getPreferences(context: Context) = context.getSharedPreferences(USER_STPRAGE, Context.MODE_PRIVATE)

    fun getLastLogin(context: Context) = getPreferences(context).getString(LAST_LOGIN, "")

    fun getLastLoginUserInfo(context: Context) = getUserInfo(context, getLastLogin(context))

    fun updateLastLogin(context: Context, login: String) {
        getPreferences(context).edit().putString(LAST_LOGIN, login).apply()
    }

    fun logout(context: Context) {
        getPreferences(context).edit().putString(LAST_LOGIN, "").apply()
    }

    fun getUserInfo(context: Context, login: String) = getPreferences(context).getString(login, "")

    fun setUserData(context: Context, user: User) {
        getPreferences(context).edit().putString(user.login, Gson().toJson(user)).apply()
    }

    fun checkLogin(context: Context, login: String) = getPreferences(context).contains(login)
}