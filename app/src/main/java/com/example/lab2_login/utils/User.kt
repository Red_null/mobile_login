package com.example.lab2_login.utils

data class User(var login: String, var password: String, var name: String = "UNKNOWN", var age: Int = 0)
