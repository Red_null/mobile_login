package com.example.lab2_login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.lab2_login.utils.Auth
import com.example.lab2_login.utils.User
import kotlinx.android.synthetic.main.activity_register.*

class Register : AppCompatActivity() {

    companion object {
        fun start(context: Context) {
            context.startActivity(Intent(context, Register::class.java))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        register_button.setOnClickListener { register() }
    }

    fun register() {

        if (!validateInputs()) {
            return
        }

        val login = login_field.text.toString()
        val password = password_field.text.toString()
        val name = name_filed.text.toString()
        val age = age_field.text.toString().toInt()

        val user = User(login, password, name, age)

        if (Auth.checkLogin(this, login)) {
            error_message.text = getString(R.string.user_exists_error)
            return
        }

        Auth.setUserData(this, user)

        Login.start(this, login)
        finish()

    }

    fun validateInputs(): Boolean {
        var validation = true
        val inputFields = arrayOf(login_field, password_field, name_filed, age_field)
        for (input in inputFields) {
            if (input.text.isNullOrEmpty()) {
                error_message.text = getString(R.string.register_form_error)
                validation = false
            }
        }
        return validation
    }

}
