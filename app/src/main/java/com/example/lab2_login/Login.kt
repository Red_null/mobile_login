package com.example.lab2_login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.lab2_login.utils.Auth
import com.example.lab2_login.utils.User
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {

    companion object {
        const val LOGIN = "login"
        fun start(context: Context, login: String? = "") {
            context.startActivity(Intent(context, Login::class.java).putExtra(LOGIN, login))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
    }

    fun init() {
        val passedLogin = intent.getStringExtra(LOGIN)
        if (passedLogin.isNotEmpty()) {
            login_field.setText(passedLogin)
            password_field.requestFocus()
        }

        login_button.setOnClickListener { login() }
        register_button.setOnClickListener { Register.start(this@Login) }
    }

    fun login() {
        val login = login_field.text.toString()
        val password = password_field.text.toString()

        val userJson = Auth.getUserInfo(this, login)
        if (userJson.isNullOrEmpty()) {
            error_label.text = getString(R.string.no_user_error)
            return
        }

        val user = Gson().fromJson(userJson, User::class.java)

        if (user.password != password) {
            error_label.text = getString(R.string.invalid_password_error)
            return
        }

        Auth.updateLastLogin(this, login)

        MainActivity.start(this, userJson)
        finish()
    }
}
